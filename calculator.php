<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Electricity Consumption Calculator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<style>
.div1 {
  font-size: 22px;
  width: 1000px;
  height: 140px;
  border-radius: 10px;
  border: 2px solid blue;
}
</style>
<body>
    <div class="container mt-5">
        <h1>Calculator</h1>
        <br><br>
        <form method="post">
            <div class="form-group">
                <label for="voltage">Voltage (V):</label>
                <input type="number" step="any" class="form-control" id="voltage" name="voltage[]" required>
            </div>
            <div class="form-group">
                <label for="current">Current (A):</label>
                <input type="number" step="any" class="form-control" id="current" name="current[]" required>
            </div>
            <div class="form-group">
                <label for="rate">Current Rate (sen/kWh):</label>
                <input type="number" step="any" class="form-control" id="rate" name="rate[]" required>
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </form>
        <br><br><br>

        <?php
        session_start();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Initialize session results array if not set
            if (!isset($_SESSION['results'])) {
                $_SESSION['results'] = [];
            }

            $voltages = $_POST["voltage"];
            $currents = $_POST["current"];
            $rates = $_POST["rate"];
            
            for ($i = 0; $i < count($voltages); $i++) {
                $voltage = $voltages[$i];
                $current = $currents[$i];
                $rate = $rates[$i];

                $power = $voltage * $current; // Power in Watts
                $energy = $power / 1000; // Energy in kWh
                $walt = $rate / 100;
                $total = $energy * $walt; // Total charge

                // Calculate hour based on the existing number of results
                $hour = count($_SESSION['results']) + 1;

                $_SESSION['results'][] = [
                    'id' => $hour,
                    'hour' => $hour,
                    'power' => $power,
                    'energy' => $energy,
                    'total' => $total
                ];
            }

            // Display the last entry's power and rate as a summary
            $lastResult = end($_SESSION['results']);
            echo "
            <center>
            <p class='div1'>
            <strong>POWER:</strong> {$energy} W
            <br><br>
            <strong>RATE :</strong> {$walt} RM
            </p>
            </center>";
        }

        echo "
        <div class='mt-5'>
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hour</th>
                        <th>Energy (kWh)</th>
                        <th>Total (RM)</th>
                    </tr>
                </thead>
                <tbody>";

                if (isset($_SESSION['results'])) {
                    foreach ($_SESSION['results'] as $result) {
                        echo "<tr>
                                <td>{$result['id']}</td>
                                <td>{$result['hour']}</td>
                                <td>{$result['energy']}</td>
                                <td>" . number_format($result['total'], 2) . "</td>
                            </tr>";
                    }
                }

        echo "
                </tbody>
            </table>
        </div>";
        ?>
    </div>

<br><br><br>
</body>

</html>